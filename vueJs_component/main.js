Vue.component(
    "product-details",
    {
        template: `
        <div class="product_details">
            <p v-if="show_description">{{description}}</p>
        </div>
        `,
        data(){
            return {
                show_description: true,
                description: "Variant product's details informations",
            }
        }
    }
)
Vue.component(
    'product-cart',
    {
        props:{
            selectedVariant:{
                type: Number,
                required: true,
                default: 0,
            },
            variants:{
                type: Array,
                required: true,
            },
            inStock:{
                type: Number,
                required:true,
            }
        },
        template: `
        <div class="product-cart">
            <div class="buttons">
                <button :class="{disabledButton: !inStock}" :disabled="!inStock" @click="addToCart">Increment cart</button>
                <button :disabled="cartEmpty" @click="removeToCart">Decrement cart</button>
            </div>
            <div class="cart"><p>Cart({{cart}})</p></div>
        </div>
        `,
        data()
        {
            return {
                cart: 0,
            }
        },
        methods:
        {
            addToCart: function(){
                if(this.inStock>0)
                    this.cart +=1;
                    this.variants[this.selectedVariant].variantQuantity--;
            },
            removeToCart()
            {
                if( this.cart > 0)
                    this.cart -= 1;
                    this.variants[this.selectedVariant].variantQuantity++;
            },
        },
        computed: {
            cartEmpty()
            {
                return this.cart <= 0;
            }
        }
    }
)
Vue.component(
    'product-list',
    {
        template: `
        <ul v-if="details.length >0">
            <li v-for="detail in details">{{detail}}</li>
        </ul>
        `,
        data(){
            return {
                details: ["80% cotton", "20% polyester", "Gender-neutral"],
            }
        }
    }
)
Vue.component(
    'product-info',
    {
        props:{
            inStock:{
                type: Number,
                required: true
            },
        },
        template: `
        <div>
            <p v-if="inStock" >In stock</p>
            <p v-else class="notAvailable">Out of stock</p>
            <p>Shipping: {{shipping}}</p>
            <product-list></product-list>
        </div>
        `,
        computed:{
            shipping(){
                return (this.premium)?"Free": "2.99€";
            }
        }

    }
)
Vue.component(
    'product-image',
    {
        props:{
            image:{
                type: String,
                required:true
            },
        },
        template: `
            <div class="product-image">
                <img :src="image" alt="Variant product image">
            </div>
        `,
    }
)

Vue.component(
    'product-color',
    {
        props:{
            variants:{
                type: Array,
                required: true
            },
            updateVariant:{
                type: Function,
                required: true,
            },
            selectedVariant:{
                type: Number,
                required: true,
            }

        },
        template:`
        <div>
            <div 
                v-for="(variant, index) in variants" 
                :key="variant.variantId"
                :style="[colorDivtyle,{backgroundColor: variant.variantColor}]"
                @mouseover="updateVariant(index)">
            </div>
        </div>
        `,
        data(){
            return{
                colorDivtyle:{
                    width: "20px",
                    height: "20px",
                    marginTop: "5px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "1em",
                    color: "#fff",
                    borderRadius: "4px",
                    cursor: "default",
                }    
            }
        }
    }

)
Vue.component(
    'product',
    {
        props: {
            premium: {
                type: Boolean,
                required: true
            }
        },
        template: `
        <div class="product">
            <product-image :image="image"></product-image>
            <div class="product_content">
                <div class="product-info">
                    <h1 class="title">{{ title }}</h1>
                    <product-info :inStock="inStock"></product-info>
                    <product-details></product-details>
                    <product-color :updateVariant="updateVariant" :selectedVariant="selectedVariant" :variants="variants"></product-color>
                </div>
                <div class="product-cart" >
                    <product-cart :inStock="inStock" :variants="variants" :selectedVariant="selectedVariant"></product-cart>
            
                </div>
            </div>
        <div>
        `,
        data(){
            return{
                brand: "Vue Mastery",
                product: "Socks",
                selectedVariant: 0,
                show_link: false,
                google_link: "https://google.fr",
                onSale: false,
                variants: [
                    {
                        variantId: 2234,
                        variantColor: "green",
                        variantImage: "./img/vmSocks-green.jpg",
                        variantQuantity: 10,
                    },
                    {
                        variantId: 2235,
                        variantColor: "blue",
                        variantImage: "./img/vmSocks-blue.jpg",
                        variantQuantity: 0,
                    },
                ],
            }
        },
        methods:{
            updateVariant: function(selectedVariant)
            {
                this.selectedVariant = selectedVariant;
            },
        },
        computed: {
            title()
            {
                return `${this.brand} ${this.product}`;
            },
            image()
            {
                return this.variants[this.selectedVariant].variantImage;
            },
            inStock()
            {
                return this.variants[this.selectedVariant].variantQuantity;
            },
        }
    }
)
var app = new Vue({
    el: '#app',
    data: {
        premium: false,
    }
})