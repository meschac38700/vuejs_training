var app = new Vue({
    el: "#app",
    data:{
        brand: "Vue Mastery",
        product: "Socks",
        selectedVariant: 0,
        description: "Description of my product",
        show_link: false,
        show_description: false,
        google_link: "https://google.fr",
        onSale: false,
        details: ["80% cotton", "20% polyester", "Gender-neutral"],
        variants: [
            {
                variantId: 2234,
                variantColor: "green",
                variantImage: "./img/vmSocks-green.jpg",
                variantQuantity: 10,
            },
            {
                variantId: 2235,
                variantColor: "blue",
                variantImage: "./img/vmSocks-blue.jpg",
                variantQuantity: 0,
            },
        ],
        cart: 0,
        colorDivtyle:{
            width: "20px",
            height: "20px",
            marginTop: "5px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            padding: "1em",
            color: "#fff",
            borderRadius: "4px",
            cursor: "default",
        }
    },
    methods:{
        incrementCart: function(){
            if(this.inStock>0)
                this.cart +=1;
                this.variants[this.selectedVariant].variantQuantity--;
        },
        decrementCart()
        {
            if( this.cart > 0)
                this.cart -= 1;
                this.variants[this.selectedVariant].variantQuantity++;
        },
        updateVariant: function(selectedVariant)
        {
            this.selectedVariant = selectedVariant;
        },



    },
    computed: {
        title()
        {
            return `${this.brand} ${this.product}`;
        },
        image()
        {
            return this.variants[this.selectedVariant].variantImage;
        },
        inStock()
        {
            return this.variants[this.selectedVariant].variantQuantity;
        },
        cartEmpty()
        {
            return this.cart <= 0;
        }
    }

})